1/12/2024
    - submitted MVP form which include the context of the product and the features that will be included within it
    - created a wireframe which is a visual representation for our app that blueprints the basic structure and layout of the app
    - wrote out the API endpoints for the app which will serve as entry points for interacting with different functionalities within the app
    - forked the project repo and built containers for the app
